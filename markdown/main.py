import enaml
from enaml.qt.qt_application import QtApplication

from model import MarkdownCode

if __name__ == "__main__":
    # View
    with enaml.imports():
        from view import Main
    # QtApp
    app = QtApplication()
    
    view = Main(content=MarkdownCode())
    view.show()
    app.start()
    
