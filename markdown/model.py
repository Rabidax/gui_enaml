from atom.api import Atom, Unicode, Bool

class MarkdownCode(Atom):
    show_source = Bool(False)
    code = Unicode("# Markdown example\nThis is _nice_, isn't it dear *user* ?")
