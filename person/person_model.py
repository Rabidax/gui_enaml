from atom.api import Atom, Unicode, Range, Bool, observe

class Person(Atom):
    """Simple class representing a Person"""
    first_name = Unicode()
    last_name = Unicode()
    age = Range(low=0)
    debug = Bool(False)

    @observe('age')
    def debug_print(self, change):
        """Print a message on age change"""
        if self.debug:
            template = "{first} {last} is {age} years old."
            output = template.format(
                first = self.first_name,
                last = self.last_name,
                age = self.age
                )
            print(output)
        
