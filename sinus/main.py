import enaml
from enaml.qt.qt_application import QtApplication

from sinus_model import Plot

if __name__ == "__main__":
    # View
    with enaml.imports():
        from sinus_view import Main
    # QtApp
    app = QtApplication()
    
    view = Main(pl=Plot())
    view.show()
    app.start()
    
