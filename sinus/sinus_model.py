from atom.api import *
import numpy as np
from matplotlib.figure import Figure

class Plot(Atom):
    def draw(self, freq):
        fig = Figure()
        ax = fig.add_subplot(111)
        space_div = np.linspace(0, 10, num=100*(freq+1))
        ax.plot(space_div, np.sin(2*np.pi*freq*space_div))
        ax.set_title(f"frequency = {freq}")
        return fig
